# Weather API

The Weather API has  a couple of use able end points that is open for developers to tinker with.

All data is formatted in JSON.

#### GET `http://domain.com/weather`

This endpoints retrives an array of weather objects with variables in given order:

* id
* temperature
* light
* created_at
* updated_at

If you do no specifi any input queries (read below) the array will be the last 100 commited weather readings from the weather station. This is done to avoid the problem of a memory max out.

Input search queries can be used such as:

* lightsAbove
* lightsBelow
* temperaturesAbove
* temperaturesBelow
* previousDays

All of them can be combined to generate a more specific search.

For exmaple if you wanted to get the readings for the last 5 days with a temperature below 40 but above 30 and light levels below 800 but above 400 the request would look like this:

`http://domain.com/weather?previousDays=5&temperaturesBelow=40&temperaturesAbove=30&lightsBelow=800&lightsAbove=400`

#### GET `http://domain.com/isOperational`

This endpoint returns a JSON object containing the variable isOperational and that vartiable can either be true or false. This request checks if the weather station has sent any information to the server in the last 60 seconds. If not the station is regarded as being down and not operational.

#### GET `http://domain.com/weather/1`

This endpoint returns the weather reading with the id of `1`. Substitute the `1` with for example `15` and you will get the weather reading of the reading with the id `15` instead.

#### PUT `http://domain.com/weather/1`

This endpoint updates the weather reading with the id of `1`. Substitute the `1` with for example `15` and you will update the weather reading of the reading with the id `15` instead.
The update able fields are `temperature` and `light`.
The updated weather reading will be returned if the creation is successful.

#### DELETE `http://domain.com/weather/1`

This endpoint deletes the weather reading with the id of `1`. Substitute the `1` with for example `15` and you will delete the weather reading of the reading with the id `15` instead.
An object with the variable `isDestroyed` will be returned with either `true` or `false`.


#### POST `http://domain.com/weather`

This endpoint creates a new weather reading with the provided values for the `temperature` and `light` field.
The created weather reading will be returned if the creation is successful.
