<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Weather;
use Log;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WeatherController extends Controller
{

    /**
    * Reports back if weather station is operational or not.
    *
    * @return Response
    */
    public function isOperational() {

        $last_weather_read = Weather::orderBy('created_at', 'desc')->first();

        $right_now = Carbon::now();

        $difference = $last_weather_read->created_at->diffInSeconds($right_now);

        if ($difference < 60) {
            return response()->json(array('isOperational' => true));
        }
        else {
            return response()->json(array('isOperational' => false));
        }

    }

    /**
    * Display a listing of weather entries.
    * It is possible to use requests to query your response.
    * previousDays=7 will return you a response from the last 7 days.
    * temperaturesAbove=25 will return you a response where temperatures are above 25.
    * temperaturesBelow=35 will return you a response where temperatures are below 35.
    * lightsAbove=200 will return you a response where lights are above 200.
    * lightsBelow=300 will return you a response where lights are below 300.
    * It is also possible to combine for example lightsAbove and lightsBelow to query a certain spectrum.
    * @param  Request  $request
    * @return Response $weathers
    */
    public function index(Request $request)
    {

        //Requests
        $previousDays       = $request->input('previousDays');
        $comingDays         = $request->input('comingDays');
        $temperaturesAbove  = $request->input('temperaturesAbove');
        $temperaturesBelow  = $request->input('temperaturesBelow');
        $lightsAbove        = $request->input('lightsAbove');
        $lightsBelow        = $request->input('lightsBelow');


        $query = Weather::query();

        if($previousDays) {
            $today = new Carbon;
            $today->subDays($previousDays);
            $query->where('created_at', '>=', $today->toDateTimeString());
        }

        if($temperaturesAbove) {
            $query->where('temperature', '>=', $temperaturesAbove);
        }

        if($temperaturesBelow) {
            $query->where('temperature', '<=', $temperaturesBelow);
        }

        if($lightsAbove) {
            $query->where('light', '>=', $lightsAbove);
        }

        if($lightsBelow) {
            $query->where('light', '<=', $lightsBelow);
        }

        $weathers = $query->orderBy('id', 'desc')->take(1000)->get();

        return response()->json($weathers);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  Request  $request
    * @return Response
    */
    public function store(Request $request)
    {

        $weather = new Weather;

        $weather->temperature = $request->input('temperature');
        $weather->light = $request->input('light');

        $weather->save();

        return response()->json($weather);

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $weather = Weather::find($id);

        return response()->json($weather);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  Request  $request
    * @param  int  $id
    * @return Response
    */
    public function update(Request $request, $id)
    {
        $weather = Weather::find($id);

        $weather->temperature = $request->input('temperature');
        $weather->light = $request->input('light');

        $weather->save();

        return response()->json($weather);

    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {

        $destroyed = Weather::destroy($id);

        return response()->json(array('isDestroyed' => $destroyed));

    }
}
