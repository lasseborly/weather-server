<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Weather;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{

  public function index() {

    return view('welcome', ['weathers' => Weather::all()]);
    
  }

}
