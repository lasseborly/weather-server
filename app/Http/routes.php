<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', [
    'as' => 'welcome', 'uses' => 'WelcomeController@index'
]);

Route::get('/isOperational', [
    'as' => 'isOperational', 'uses' => 'WeatherController@isOperational'
]);

//http://laravel.com/docs/5.1/controllers#restful-resource-controllers
Route::resource('weather', 'WeatherController');

Route::get('/phpinfo', function () {
    return view('phpinfo');
});
