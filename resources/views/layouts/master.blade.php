<html>
<head>
  <title>Weather - @yield('title')</title>

  <!--  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/js/materialize.min.js"></script>

</head>
<body>

  <h1 class="center-align">Weather</h1>

  <div class="container">
    @yield('content')
  </div>

</body>
</html>
