@extends('layouts.master')

@section('title', 'Welcome')

@section('content')
<div class="row">
  <div class="col l4 offset-l4">

    <table class="responsive-table bordered centered">
      <thead>
        <tr>
          <th data-field="time">Time</th>
          <th data-field="temperature">Temperature</th>
          <th data-field="light">Light</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($weathers as $weather)
          <tr>
            <td>{{$weather->created_at}}</td>
            <td>{{$weather->temperature}} C</td>
            <td>{{$weather->light}} lm</td>
          </tr>
        @endforeach

      </tbody>
    </table>

  </div>
</div>
@endsection
