<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WeatherTest extends TestCase
{
    /**
    * Checks if a new weather record can be stored.
    *
    * @return void
    */
    public function store()
    {
        $this->post('/weather', ['temperature' => 50, 'light' => 200])
        ->seeJson([
            'created' => true,
            ]);
    }

    /**
    * Checks if a weather record can be updated.
    *
    * @return void
    */
    public function update()
    {
        $this->put('/weather/1', ['temperature' => 40, 'light' => 300])
        ->seeJson([
            'created' => true,
            ]);
    }

    /**
    * Checks if a weather record can be retrieved.
    *
    * @return void
    */
    public function show()
    {
        $this->get('/weather/1')
        ->seeJson([
            'created' => true,
            ]);
    }

    /**
    * Checks if weather records can be retrieved.
    *
    * @return void
    */
    public function index()
    {
        $this->get('/weather')
        ->seeJson([
            'created' => true,
            ]);
    }

    /**
    * Checks if a weather record can be destroyed.
    *
    * @return void
    */
    public function destroy()
    {
        $this->delete('/weather/1')
        ->seeJsonEquals([
            'isDestroyed' => true,
             ]);
    }

    /**
    * Checks if a the weather station can be checked for operationality.
    *
    * @return void
    */
    public function isOperational()
    {
        $this->get('/isOperational')
        ->seeJson([
            'created' => true,
             ]);
    }

}
